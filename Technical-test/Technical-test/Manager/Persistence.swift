//
//  Persistence.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import CoreData

struct PersistenceController {
    
    // MARK: - Public Variables
    
    static let shared = PersistenceController()

    let container: NSPersistentContainer
    
    var context: NSManagedObjectContext {
        container.viewContext
    }

    // MARK: - Initialization
    
    init(inMemory: Bool = false) {
        container = NSPersistentContainer(name: "QuotesList")
        container.viewContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        
        if inMemory {
            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
        }
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        
        container.viewContext.automaticallyMergesChangesFromParent = true
    }
}
