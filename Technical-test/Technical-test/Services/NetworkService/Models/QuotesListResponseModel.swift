//
//  QuotesListResponseModel.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import Foundation

struct QuotesListResponseModel: Codable {
    let symbol: String?
    let name: String?
    let currency: String?
    let readableLastChangePercent: String?
    let last: String?
    let variationColor: String?
    let key: String?
}
