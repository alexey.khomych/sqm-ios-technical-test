//
//  QuotesPoint.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import Foundation

typealias QuotesListResponseCompletion = (
    _ model: [QuotesListResponseModel]?,
    _ error: Error?
) -> ()

protocol QuotesAPIPoint {
    func list(completion: @escaping QuotesListResponseCompletion)
}

final class QuotesAPIPointImpl: QuotesAPIPoint {
    
    // MARK: - Private Properties
    
    private let router = NetworkServiceImpl<QuotesEndPoint>()
    
    // MARK: - Public
    
    func list(completion: @escaping QuotesListResponseCompletion) {
        router.request(.list) { response in
            DispatchQueue.main.async {
                let handledResponse = response.handle([QuotesListResponseModel].self)
                completion(handledResponse.data, handledResponse.error)
            }
        }
    }
}
