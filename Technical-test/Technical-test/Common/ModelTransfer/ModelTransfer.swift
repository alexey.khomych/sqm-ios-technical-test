//
//  ModelTransfer.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import Foundation

/**
 Uses for processing model by view
 ```
 extension SomeViewCell: ModelTransfer {
     
     func update(with model: SomeModel) {
         titleLabel.text = model.title
     }
 }
 ```
 */
protocol ModelTransfer {
    /// Type of model that is being transferred
    associatedtype ModelType

    /// Updates view with `model`.
    func update(with model: ModelType)
}
