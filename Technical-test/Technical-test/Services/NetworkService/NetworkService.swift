//
//  NetworkService.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import Foundation

protocol NetworkService {
    associatedtype EndPoint: EndPointType
    
    func request(_ route: EndPoint, completion: @escaping (_ response: HTTPResponse) -> ())
}

class NetworkServiceImpl<EndPoint: EndPointType>: NetworkService {
    
    // MARK: - Private Properties
    
    private var task: URLSessionTask?
    
    // MARK: - Public
    
    func request(_ route: EndPoint, completion: @escaping (_ response: HTTPResponse) -> ()) {
        do {
            let request = try self.buildRequest(from: route)
            
            task = URLSession.shared.dataTask(with: request) { data, response, error in
                let response = HTTPResponse(data: data, response: response, error: error)
                completion(response)
            }
            
            task?.resume()
        } catch {
            let response = HTTPResponse(data: nil, response: nil, error: error)
            completion(response)
        }
    }
}

// MARK: - Private

private extension NetworkServiceImpl {
    
    func buildRequest(from route: EndPoint) throws -> URLRequest {
        var request = URLRequest(
            url: route.baseUrl.appendingPathComponent(route.path),
            cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 10
        )
        
        request.httpMethod = route.httpMethod.rawValue
        
        if let headers = route.headers {
            addAdditionalHeaders(headers, &request)
        }
        
        do {
            switch route.task {
                case .request:
                    break
                case .requestParameters(
                    let bodyParameters,
                    let bodyEncoding,
                    let urlParameters
                ):
                    
                    try configureParameters(
                        bodyParameters: bodyParameters,
                        bodyEncoding: bodyEncoding,
                        urlParameters: urlParameters,
                        request: &request
                    )
                case .requestParametersAndHeaders(
                    let bodyParameters,
                    let bodyEncoding,
                    let urlParameters,
                    let additionalHeaders
                ):
                    addAdditionalHeaders(additionalHeaders, &request)
                    try configureParameters(
                        bodyParameters: bodyParameters,
                        bodyEncoding: bodyEncoding,
                        urlParameters: urlParameters,
                        request: &request
                    )
            }
            return request
        } catch {
            throw error
        }
    }
    
    func configureParameters(
        bodyParameters: Parameters?,
        bodyEncoding: ParameterEncoding,
        urlParameters: Parameters?,
        request: inout URLRequest
    ) throws {
        do {
            try bodyEncoding.encode(
                urlRequest: &request,
                bodyParameters: bodyParameters,
                urlParameters: urlParameters
            )
        } catch {
            throw error
        }
    }
    
    func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, _ request: inout URLRequest) {
        guard let headers = additionalHeaders else { return }
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
}
