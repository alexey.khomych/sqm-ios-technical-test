//
//  NetworkError.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import Foundation

public enum NetworkError: String, Error {
    case encodingFailed = "Parameters encoding failed."
    case missingUrl = "URL is nil."
    case badRequest = "Invalid request"
    case serverError = "Server Error"
    case uknownError = "Uknown Error"
    case clientError = "Client error responses"
    case redirectError = "Reddirect error"
}
