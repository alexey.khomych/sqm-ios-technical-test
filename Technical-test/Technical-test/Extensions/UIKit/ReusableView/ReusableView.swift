//
//  ReusableView.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import UIKit

protocol ReusableView {
    static func reuseIdentifier() -> String
}

extension ReusableView {
    static func reuseIdentifier() -> String {
        return String(describing: self)
    }
}

// MARK: - UITableViewCell

extension ReusableView where Self: UITableViewCell {
    
    static func registerFor(tableView: UITableView) {
        tableView.register(Self.self, forCellReuseIdentifier: Self.reuseIdentifier())
    }
}

// MARK: - ReusableView

extension UITableViewCell: ReusableView {}
