//
//  UIViewController+Alert.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import UIKit

extension UIViewController {
    
    func presentAlert(_ message: String) {
        let dialogMessage = UIAlertController(
            title: "Confirm",
            message: message,
            preferredStyle: .alert
        )
        
        present(dialogMessage, animated: true, completion: nil)
    }
}
