//
//  DataManager.swift
//  Technical-test
//
//  Created by Patrice MIAKASSISSA on 29.04.21.
//

import Foundation
import CoreData

typealias DataManagerFetchQuotesResultCompletion = (Result<[Quote], Error>) -> ()
typealias DataManagerStatusUpdateCompletionHandler = (_ error: String?) -> ()

final class DataManager {
    
    // MARK: - Private Properties
    
    private let networkService = QuotesAPIPointImpl()
    
    // MARK: - Public
    
    func fetchQuotes(completion: @escaping DataManagerFetchQuotesResultCompletion) {
        let dispatchGroup = DispatchGroup()
        
        var storedError: Error?
        var quotesList: [Quote]?
        var favoriteQuotesList: [Favorite]?
        
        let fetchRequest = Favorite.fetchRequest()
        
        dispatchGroup.enter()
        networkService.list { model, error in
            defer { dispatchGroup.leave() }
            
            storedError = error
            quotesList = self.convertResponseModelToDataSourceModel(model ?? [])
        }
        
        dispatchGroup.enter()
        
        do {
            defer { dispatchGroup.leave() }
            
            favoriteQuotesList = try PersistenceController.shared.context.fetch(fetchRequest)
            
        } catch {
            storedError = error
        }
        
        dispatchGroup.notify(queue: .main) {
            if let storedError {
                completion(.failure(storedError))
                
                return
            }
            
            guard let quotesList, let favoriteQuotesList else {
                completion(.failure(NetworkError.uknownError))
                
                return
            }
            
            self.applySavedFavoriteState(quotesList: quotesList, favoriteList: favoriteQuotesList)
            
            completion(.success(quotesList))
        }
    }
    
    func updateState(for quote: Quote, completion: DataManagerStatusUpdateCompletionHandler) {
        if quote.isFavorite == nil {
            quote.isFavorite = true
        } else {
            quote.isFavorite?.toggle()
        }
        
        guard let key = quote.key else { return }
        
        do {
            let favorite = Favorite(context: PersistenceController.shared.context)
            favorite.quoteId = key
            favorite.isFavorite = quote.isFavorite!
            
            try PersistenceController.shared.context.save()
            
            completion(nil)
        } catch {
            completion(error.localizedDescription)
        }
    }
}

// MARK: - Private

private extension DataManager {
    
    func convertResponseModelToDataSourceModel(_ model: [QuotesListResponseModel]) -> [Quote] {
        model.map { Quote.makeQuoteFromResponseModel($0) }
    }
    
    func applySavedFavoriteState(quotesList: [Quote], favoriteList: [Favorite]) {
        favoriteList.forEach { favorite in
            guard let quote = quotesList.first(where: { $0.key == favorite.quoteId }) else { return }
            quote.isFavorite = favorite.isFavorite
        }
    }
}
