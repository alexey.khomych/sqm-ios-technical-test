//
//  QuotesListItemCell.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import UIKit

class QuotesListItemCell: UITableViewCell {
    
    // MARK: - Private Properties
    
    private let containerStackView = UIStackView()
    private let infoContainerStackView = UIStackView()
    
    private let nameLabel = UILabel()
    private let lastLabel = UILabel()
    private let lastChangePercentLabel = UILabel()
    private let favoriteImageView = UIImageView()
    
    // MARK: - Initializations
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        containerStackView.distribution = .fillProportionally
        containerStackView.axis = .horizontal
        infoContainerStackView.axis = .vertical
        containerStackView.spacing = 10
        
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
        infoContainerStackView.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        lastLabel.translatesAutoresizingMaskIntoConstraints = false
        lastChangePercentLabel.translatesAutoresizingMaskIntoConstraints = false
        favoriteImageView.translatesAutoresizingMaskIntoConstraints = false
        
        nameLabel.font = UIFont.systemFont(ofSize: 14)
        lastLabel.font = UIFont.systemFont(ofSize: 14)
        lastChangePercentLabel.font = UIFont.systemFont(ofSize: 16)
        
        lastChangePercentLabel.textAlignment = .center
        
        let imageContainerView = UIView()
        imageContainerView.translatesAutoresizingMaskIntoConstraints = false
        imageContainerView.addSubview(favoriteImageView)
        
        favoriteImageView.image = UIImage(named: "no-favorite")
        
        infoContainerStackView.addArrangedSubview(nameLabel)
        infoContainerStackView.addArrangedSubview(lastLabel)
        
        nameLabel.setContentHuggingPriority(.init(252), for: .vertical)
        
        containerStackView.addArrangedSubview(infoContainerStackView)
        containerStackView.addArrangedSubview(lastChangePercentLabel)
        containerStackView.addArrangedSubview(imageContainerView)
        
        contentView.addSubview(containerStackView)
        
        let contentLayoutMarginsGuide = contentView.layoutMarginsGuide
        
        NSLayoutConstraint.activate(
            [
                containerStackView.topAnchor.constraint(equalTo: contentLayoutMarginsGuide.topAnchor, constant: 10),
                containerStackView.leadingAnchor.constraint(equalTo: contentLayoutMarginsGuide.leadingAnchor, constant: 0),
                containerStackView.trailingAnchor.constraint(equalTo: contentLayoutMarginsGuide.trailingAnchor, constant: 0),
                containerStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
                
                infoContainerStackView.widthAnchor.constraint(equalToConstant: 150),
                
                favoriteImageView.centerYAnchor.constraint(equalTo: imageContainerView.centerYAnchor),
                favoriteImageView.heightAnchor.constraint(equalToConstant: 22),
                favoriteImageView.widthAnchor.constraint(equalToConstant: 22),
                favoriteImageView.trailingAnchor.constraint(equalTo: imageContainerView.trailingAnchor, constant: 0),
            ]
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Lifecycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        contentView.layer.borderWidth = 0
        contentView.layer.borderColor = UIColor.clear.cgColor
        favoriteImageView.image = UIImage(named: "no-favorite")
        lastChangePercentLabel.textColor = .black
    }
}

// MARK: - ModelTransfer

extension QuotesListItemCell: ModelTransfer {
    
    func update(with model: Quote) {
        nameLabel.text = model.name
        lastLabel.text = "\(model.last!) \(model.currency!)"
        
        if let isFavorite = model.isFavorite {
            favoriteImageView.image = UIImage(named: isFavorite ? "favorite" : "no-favorite")
            contentView.layer.borderWidth = isFavorite ? 1 : 0
            contentView.layer.borderColor = isFavorite ? UIColor.blue.cgColor : UIColor.clear.cgColor
        }
        
        lastChangePercentLabel.text = model.readableLastChangePercent
        
        if let variationColor = model.variationColor {
            lastChangePercentLabel.textColor = UIColor(named: variationColor)
        }
    }
}
