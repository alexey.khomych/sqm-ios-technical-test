//
//  QuotesListViewController.swift
//  Technical-test
//
//  Created by Patrice MIAKASSISSA on 29.04.21.
//

import UIKit

class QuotesListViewController: UIViewController {
    
    // MARK: - Private Properties
    
    private let dataManager = DataManager()
    
    private let tableView = UITableView()
    private let dataSource = ArrayTableViewDataSource<Quote, QuotesListItemCell>()
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
        configureUI()
    }
}

// MARK: - Private

private extension QuotesListViewController {
    
    func loadData() {
        dataManager.fetchQuotes() { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
                case let .success(model):
                    self.dataSource.updateWith(items: model)
                    self.tableView.reloadData()
                case let .failure(error):
                    self.presentAlert(error.localizedDescription)
            }
        }
    }
    
    func configureUI() {
        prepareTableView()
        registerCells()
        
        view.backgroundColor = .white
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Quotes"
    }
    
    func prepareTableView() {
        tableView.dataSource = dataSource
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(tableView)
        
        let safeArea = view.safeAreaLayoutGuide
        
        NSLayoutConstraint.activate(
            [
                tableView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 0),
                tableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 0),
                tableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: 0),
                tableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: 0)
            ]
        )
    }
    
    func registerCells() {
        QuotesListItemCell.registerFor(tableView: tableView)
    }
}

// MARK: - UITableViewDelegate

extension QuotesListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = dataSource.items[indexPath.row]
        let detailsViewController = QuoteDetailsViewController(quote: model)
        navigationController?.pushViewController(detailsViewController, animated: true)
    }
}
