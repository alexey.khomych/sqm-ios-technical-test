//
//  HTTPResponse.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import Foundation

struct HTTPResponse {
    
    // MARK: - Public Properties
    
    let data: Data?
    let response: URLResponse?
    let error: Error?
    
    // MARK: - Public
    
    func handle<T>(_ type: T.Type) -> (data: T?, error: Error?) where T : Decodable {
        guard let response = response as? HTTPURLResponse else {
            return (nil, NetworkError.badRequest)
        }
        
        if let statusCodeError = response.statusCode() {
            return (nil, statusCodeError)
        }
        
        guard let responseData = data else {
            return (nil, NetworkError.badRequest)
        }
        
        do {
            let apiResponse = try JSONDecoder().decode(type, from: responseData)
            
            return (apiResponse, nil)
        } catch {
            return (nil, NetworkError.encodingFailed)
        }
    }
}
