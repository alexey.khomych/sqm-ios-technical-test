//
//  QuotesEndPoint.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import Foundation

public enum QuotesEndPoint {
    case list
}

// MARK: - EndPointType

extension QuotesEndPoint: EndPointType {
    
    var path: String {
        switch self {
        case .list:
            return "/mobile/iphone/Quote.action"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .list:
            return .get
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .list:
            return .requestParameters(
                bodyParameters: nil,
                bodyEncoding: .urlEncoding,
                urlParameters: [
                    "formattedList" : true,
                    "listType" : "SMI",
                    "addServices" : true,
                    "updateCounter" : true,
                    "s" : "$smi",
                    "lastTime" : 0,
                    "api" : 2,
                    "framework" : "6.1.1",
                    "format" : "json",
                    "locale" : "en",
                    "mobile" : "iphone",
                    "language" : "en",
                    "version" : "80200",
                    "formatNumbers" : true,
                    "mid" : "5862297638228606086",
                    "wl" : "sq"
                ]
            )
        }
    }
}
