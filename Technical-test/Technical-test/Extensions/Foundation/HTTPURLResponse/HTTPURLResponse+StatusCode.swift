//
//  HTTPURLResponse.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import Foundation

extension HTTPURLResponse {
    
    ///parse error code and return localized error message
    func statusCode() -> NetworkError? {
        switch statusCode {
            case 200...299:
                return nil
            case 300...399:
                return .redirectError
            case 400:
                return .serverError
            case 401...500:
                return .clientError
            case 501...599:
                return .serverError
            default:
                return .uknownError
        }
    }
}
