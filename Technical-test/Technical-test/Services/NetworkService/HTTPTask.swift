//
//  HTTPTask.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import Foundation

public typealias HTTPHeaders = [String: String]

enum HTTPTask {
    case request
    
    case requestParameters(
        bodyParameters: Parameters?,
        bodyEncoding: ParameterEncoding,
        urlParameters: Parameters?
    )
    
    case requestParametersAndHeaders(
        bodyParameters: Parameters?,
        bodyEncoding: ParameterEncoding,
        urlParameters: Parameters?,
        additionalHeaders: HTTPHeaders?
    )
}
