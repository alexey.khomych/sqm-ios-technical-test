//
//  UITableView+dequeueReusableCell.swift
//  Technical-test
//
//  Created by Alexey Khomych on 08.04.2023.
//

import UIKit

extension UITableView {

    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier(), for: indexPath) as? T else {
            #if DEBUG
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier())")
            #else
            return T()
            #endif
        }
        
        return cell
    }
}
