//
//  Quote.swift
//  Technical-test
//
//  Created by Patrice MIAKASSISSA on 29.04.21.
//

import Foundation

class Quote {
    
    // MARK: - Public Properties
    
    var symbol: String?
    var name: String?
    var currency: String?
    var readableLastChangePercent: String?
    var last: String?
    var variationColor: String?
    var isFavorite: Bool?
    var key: String?
    
    // MARK: - Initialization
    
    init(
        symbol: String? = nil,
        name: String? = nil,
        currency: String? = nil,
        readableLastChangePercent: String? = nil,
        last: String? = nil,
        variationColor: String? = nil,
        isFavorite: Bool? = nil,
        key: String? = nil
    ) {
        self.symbol = symbol
        self.name = name
        self.currency = currency
        self.readableLastChangePercent = readableLastChangePercent
        self.last = last
        self.variationColor = variationColor
        self.isFavorite = isFavorite
        self.key = key
    }
}

// MARK: - Equatable

extension Quote: Equatable {
    
    static func == (lhs: Quote, rhs: Quote) -> Bool {
        lhs.symbol == rhs.symbol
        && lhs.name == rhs.name
        && lhs.currency == rhs.currency
        && lhs.readableLastChangePercent == rhs.readableLastChangePercent
        && lhs.last == rhs.last
        && lhs.variationColor == rhs.variationColor
        && lhs.isFavorite == rhs.isFavorite
        && lhs.key == rhs.key
    }
}

extension Quote {
    
    static func makeQuoteFromResponseModel(_ model: QuotesListResponseModel) -> Quote {
        let randomNum = Int.random(in: 1...2)
        
        return Quote(
            symbol: model.symbol,
            name: model.name,
            currency: model.currency,
            readableLastChangePercent: model.readableLastChangePercent,
            last: model.last,
            variationColor: model.variationColor,
            isFavorite: nil,
            key: model.key
        )
    }
}
